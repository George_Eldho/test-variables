FROM node:14.17.0-alpine3.10

WORKDIR /app

ARG envv

ENV envv=$envv

COPY . .

RUN echo $envv


CMD ["npm", "run", "start:${envv}"]